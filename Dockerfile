FROM python:3.11.3-slim-bullseye

RUN apt-get update \
 && pip install --upgrade pip \
    pip install flask \
 && rm -rf /var/lib/apt/lists/*

COPY app.py /opt/app.py

ENTRYPOINT FLASK_APP=/opt/app.py flask run --host=0.0.0.0 --port=5000
